package com.spacexrocketlaunches.http

import retrofit2.Response
import javax.inject.Inject

class DefaultExceptionResolver @Inject constructor() : ExceptionResolver {

    override fun <T> resolveError(response: Response<T>): Throwable {
        // TODO add proper handling
        return IllegalStateException(response.errorBody()!!.string())
    }
}