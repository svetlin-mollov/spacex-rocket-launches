package com.spacexrocketlaunches.http

import retrofit2.Response

interface ExceptionResolver {

    fun <T> resolveError(response: Response<T>): Throwable
}