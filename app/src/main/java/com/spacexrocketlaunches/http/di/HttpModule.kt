package com.spacexrocketlaunches.http.di

import com.spacexrocketlaunches.BuildConfig
import com.spacexrocketlaunches.http.DefaultExceptionResolver
import com.spacexrocketlaunches.http.ExceptionResolver
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppProviderModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class BaseUrl

    @BaseUrl
    @Singleton
    @Provides
    fun provideBaseUrl(): String {
        return "https://api.spacexdata.com"
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .also {
                if (BuildConfig.DEBUG) {
                    it.addInterceptor(
                        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
                    )
                }
            }
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(
        @BaseUrl baseUrl: String,
        okHttpClient: OkHttpClient,
    ): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .build()
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class AppBinderModule {

    @Singleton
    @Binds
    abstract fun bindExceptionResolver(
        defaultExceptionResolver: DefaultExceptionResolver
    ): ExceptionResolver
}