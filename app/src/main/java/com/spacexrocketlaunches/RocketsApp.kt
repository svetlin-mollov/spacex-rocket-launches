package com.spacexrocketlaunches

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RocketsApp : Application()