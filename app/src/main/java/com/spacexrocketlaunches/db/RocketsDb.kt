package com.spacexrocketlaunches.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.spacexrocketlaunches.module.launches.datasource.local.LaunchesDao
import com.spacexrocketlaunches.module.launches.datasource.local.entity.LaunchEntity
import com.spacexrocketlaunches.module.rockets.datasource.local.RocketsDao
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.RocketEntity

@Database(
    entities = [
        RocketEntity::class,
        LaunchEntity::class,
    ],
    version = 1,
    exportSchema = false,
)
abstract class RocketsDb : RoomDatabase() {

    abstract fun rocketsDao(): RocketsDao

    abstract fun launchesDao(): LaunchesDao
}