package com.spacexrocketlaunches.db.di

import android.content.Context
import androidx.room.Room
import com.spacexrocketlaunches.db.RocketsDb
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DbModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): RocketsDb {
        return Room.databaseBuilder(
            context.applicationContext,
            RocketsDb::class.java,
            "Rockets.db"
        ).build()
    }
}