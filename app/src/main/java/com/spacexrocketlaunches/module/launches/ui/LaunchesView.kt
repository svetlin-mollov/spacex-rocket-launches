package com.spacexrocketlaunches.module.launches.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.mollov.util.Diff
import com.mollov.util.listBuilder
import com.mollov.util.stickyheaders.StickyHeaderDecoration
import com.mollov.util.view.StatefulView
import com.spacexrocketlaunches.R
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import com.spacexrocketlaunches.module.launches.ui.item.*
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter
import com.trading212.diverserecycleradapter.util.replaceItems
import kotlinx.android.synthetic.main.view_launches.view.*

class LaunchesView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : StatefulView<LaunchesState>(context, attrs, defStyleAttr) {

    var onRefresh: () -> Unit = {}

    var onBackPressed: () -> Unit = {}

    private val adapter = DiverseRecyclerAdapter().also {
        it.setHasStableIds(true)
    }

    override fun createView(inflater: LayoutInflater, parent: ViewGroup): View =
        inflater.inflate(R.layout.view_launches, parent, false)

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(StickyHeaderDecoration())

        swipeToRefreshLayout.setOnRefreshListener {
            onRefresh()
        }
    }

    override fun render(state: LaunchesState, diff: Diff<LaunchesState>) {

        if (diff.by { it.isLoading }) {
            progressIndicator.isVisible = state.isLoading && state.launches == null

            val isRefreshing = state.isLoading && state.launches != null
            swipeToRefreshLayout.post {
                swipeToRefreshLayout.isRefreshing = isRefreshing
            }
        }

        if (diff.by { it.rocket?.name }) {
            toolbar.title = state.rocket?.name
        }

        if (diff.by { it.rocket?.image }) {

            imageView.transitionName = state.rocket?.image

            Glide
                .with(context)
                .load(state.rocket?.image)
                .placeholder(R.drawable.ic_rocket_placeholder)
                .centerCrop()
                .into(imageView)
        }

        if (diff.by { it.rocket } || diff.byList { it.launches }) {
            adapter.replaceItems(buildDetails(state.rocket, state.launches))
        }
    }

    private fun buildDetails(
        rocket: Rocket?,
        launches: List<Launch>?,
    ): List<DiverseRecyclerAdapter.RecyclerItem<*, *>> {
        rocket ?: return emptyList()

        return listBuilder {

            +MeasuresRecyclerItem(
                MeasuresRecyclerItem.Data(
                    diameter = rocket.diameter,
                    height = rocket.height,
                    mass = rocket.mass,
                    enginesCount = rocket.enginesCount,
                    flights = launches?.size,
                )
            )

            +DescriptionRecyclerItem(
                DescriptionRecyclerItem.Data(
                    key = context.getString(R.string.country),
                    value = rocket.country,
                )
            )

            +DescriptionRecyclerItem(
                DescriptionRecyclerItem.Data(
                    key = context.getString(R.string.first_flight),
                    value = rocket.firstFlight, // TODO Format the date
                )
            )

            +DescriptionRecyclerItem(
                DescriptionRecyclerItem.Data(
                    key = context.getString(R.string.active),
                    value = context.getString(if (rocket.isActive) R.string.yes else R.string.no),
                )
            )

            +DescriptionRecyclerItem(
                DescriptionRecyclerItem.Data(
                    key = context.getString(R.string.wiki),
                    value = rocket.wikipedia,
                )
            )

            +DescriptionRecyclerItem(
                DescriptionRecyclerItem.Data(
                    key = context.getString(R.string.description),
                    value = rocket.description,
                )
            )

            if (launches?.takeUnless { it.isEmpty() } != null) {

                // TODO parse the date, extract the year and then perform the grouping
                val launchesByYear = launches.groupBy { it.launchDate.split("-").first() }

                +StickyHeaderRecyclerItem(context.getString(R.string.launches_per_year))

                +LaunchesGraphRecyclerItem(launchesByYear)

                launchesByYear.forEach { (year, launches) ->
                    +StickyHeaderRecyclerItem(year) // TODO Format the date

                    launches.forEach {
                        +LaunchRecyclerItem(it)
                    }
                }
            }
        }
    }
}