package com.spacexrocketlaunches.module.launches.datasource.remote

import com.spacexrocketlaunches.module.launches.datasource.remote.dto.LaunchesRequest
import com.spacexrocketlaunches.module.launches.datasource.remote.dto.LaunchesResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LaunchesRetrofitService {

    @POST("/v4/launches/query")
    suspend fun getLaunches(@Body request: LaunchesRequest): Response<LaunchesResponse>
}