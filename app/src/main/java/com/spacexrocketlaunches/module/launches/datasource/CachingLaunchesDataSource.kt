package com.spacexrocketlaunches.module.launches.datasource

import com.mollov.util.Result
import com.spacexrocketlaunches.module.launches.di.LaunchesBinderModule
import com.spacexrocketlaunches.module.launches.domain.LaunchesRepository
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import javax.inject.Inject

class CachingLaunchesDataSource @Inject constructor(
    @LaunchesBinderModule.RemoteLaunchesDataSource
    private val launchesRemoteDataSource: LaunchesDataSource,

    @LaunchesBinderModule.LocalLaunchesDataSource
    private val launchesLocalDataSource: LaunchesDataSource,
) : LaunchesRepository {

    override suspend fun getLaunches(rocketId: String): Result<List<Launch>> {

        val remoteResult = launchesRemoteDataSource.getLaunches(rocketId)

        return if (remoteResult is Result.Success) {
            launchesLocalDataSource.saveLaunches(remoteResult.data)

            remoteResult
        } else {
            launchesLocalDataSource.getLaunches(rocketId)
        }
    }
}