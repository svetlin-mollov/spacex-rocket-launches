package com.spacexrocketlaunches.module.launches.datasource.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.RocketEntity

@Entity(
    foreignKeys = [ForeignKey(
        entity = RocketEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("rocketId"),
        onDelete = ForeignKey.CASCADE
    )]
)
class LaunchEntity(

    @PrimaryKey
    val id: String,

    val name: String,

    val details: String,

    val launchDate: String,

    val success: Boolean,

    val missionPatchUrl: String,

    @ColumnInfo(index = true)
    val rocketId: String,
)