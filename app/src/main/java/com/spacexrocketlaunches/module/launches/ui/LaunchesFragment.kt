package com.spacexrocketlaunches.module.launches.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.mollov.util.whileIn
import com.spacexrocketlaunches.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LaunchesFragment : Fragment() {

    private val args: LaunchesFragmentArgs by navArgs()

    private val viewModel: LaunchesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_launches, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO inspect why the animation is broken
//        sharedElementEnterTransition = TransitionInflater
//            .from(requireContext())
//            .inflateTransition(android.R.transition.move)

        viewModel.rocket = args.rocket
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val launchesView = view.findViewById<LaunchesView>(R.id.launchesView).apply {
            onRefresh = { viewModel.refresh() }
            onBackPressed = { findNavController().popBackStack() }
        }

        viewLifecycleOwner.whileIn(Lifecycle.State.STARTED) {

            // Consume state changes in a new coroutine
            launch {
                viewModel.state.collect { launchesView.state = it }
            }
        }
    }
}