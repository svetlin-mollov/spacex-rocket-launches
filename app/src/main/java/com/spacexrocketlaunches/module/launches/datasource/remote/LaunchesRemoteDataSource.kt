package com.spacexrocketlaunches.module.launches.datasource.remote

import com.mollov.util.Result
import com.spacexrocketlaunches.http.ExceptionResolver
import com.spacexrocketlaunches.module.launches.datasource.LaunchesDataSource
import com.spacexrocketlaunches.module.launches.datasource.remote.dto.LaunchesRequest
import com.spacexrocketlaunches.module.launches.datasource.remote.dto.Query
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LaunchesRemoteDataSource @Inject constructor(
    private val service: LaunchesRetrofitService,
    private val mapper: LaunchMapper,
    private val exceptionResolver: ExceptionResolver,
    private val coroutineDispatcher: CoroutineDispatcher,
) : LaunchesDataSource {

    override suspend fun saveLaunches(launches: List<Launch>): Result<Unit> {

        // Just return success, because the remote storage is not available
        return Result.Success(Unit)
    }

    override suspend fun getLaunches(rocketId: String): Result<List<Launch>> {
        return withContext(coroutineDispatcher) {

            try {
                val response = service.getLaunches(LaunchesRequest(Query(rocketId)))

                val body = response.body()
                if (response.isSuccessful && body != null) {
                    Result.Success(
                        body.docs.map { mapper.mapFromLaunchDto(it) }
                    )
                } else {
                    Result.Error(
                        exceptionResolver.resolveError(response)
                    )
                }
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }
}