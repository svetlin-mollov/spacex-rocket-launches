package com.spacexrocketlaunches.module.launches.domain.model

data class Launch(
    val id: String,
    val name: String,
    val details: String,
    val launchDate: String,
    val success: Boolean,
    val missionPatchUrl: String,
    val rocketId: String,
)
