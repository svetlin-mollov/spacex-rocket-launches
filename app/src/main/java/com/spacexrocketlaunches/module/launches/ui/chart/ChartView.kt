package com.spacexrocketlaunches.module.launches.ui.chart

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import com.mollov.util.dpF

/**
 * A very hardcoded, non-customizable, dummy, simple chart that does the work.
 */
class ChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    // Year -> Number of launches
    var launchesPerYear: Map<String, Int>? = null
        set(value) {
            field = value

            launchesYears = value?.keys?.toList()
            launchesCount = value?.values?.toList()

            invalidate()
        }

    private var launchesYears: List<String>? = null
    private var launchesCount: List<Int>? = null

    private val linePath = Path()
    private val linePaint = Paint().also {
        it.isAntiAlias = true
        it.style = Paint.Style.STROKE
        it.strokeCap = Paint.Cap.ROUND
        it.strokeJoin = Paint.Join.ROUND
        it.strokeWidth = 4.dpF
        it.color = Color.LTGRAY
    }

    private val axisPaint = Paint().also {
        it.style = Paint.Style.STROKE
        it.strokeWidth = 2.dpF
        it.color = Color.LTGRAY
    }

    private val textPaint = Paint().also {
        it.isAntiAlias = true
        it.textSize = 13.dpF
        it.color = Color.LTGRAY
    }

    private val lineOffset = 40.dpF

    private val axisOffset = 20.dpF

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas ?: return

        val width = width.toFloat()
        val height = height.toFloat()

        // X axis
        canvas.drawLine(
            axisOffset, height - axisOffset,
            width, height - axisOffset,
            axisPaint,
        )

        // Y axis
        canvas.drawLine(
            axisOffset, height - axisOffset,
            axisOffset, 0f,
            axisPaint,
        )

        val launchesYears = launchesYears ?: return
        val launchesCount = launchesCount ?: return

        // Line, years and number of launches

        linePath.reset()

        val lineContainerWidth = width - 2 * lineOffset
        val lineContainerHeight = height - 2 * lineOffset

        val partWidth = lineContainerWidth / launchesCount.lastIndex

        launchesCount
            .asSequence()
            .normalize()
            ?.forEachIndexed { index, value ->
                val x = index * partWidth + lineOffset
                val y = (1f - value) * lineContainerHeight + lineOffset

                if (index == 0) {
                    linePath.moveTo(x, y)
                } else {
                    linePath.lineTo(x, y)
                }

                // Launch year
                canvas.drawText(
                    launchesYears[index],
                    x - textPaint.measureText(launchesYears[index]) / 2f,
                    height,
                    textPaint
                )

                // Launches count that year
                canvas.drawText(
                    launchesCount[index].toString(),
                    0f,
                    y,
                    textPaint
                )
            }

        canvas.drawPath(linePath, linePaint)
    }
}

/**
 * Transforms Int numbers into the range of [0f, 1f]
 */
private fun Sequence<Int>.normalize(): Sequence<Float>? {
    val min = this.minOrNull() ?: return null
    val max = this.maxOrNull() ?: return null

    return this
        .map { (it - min) / (max - min) }
        .map { it.toFloat() }
}