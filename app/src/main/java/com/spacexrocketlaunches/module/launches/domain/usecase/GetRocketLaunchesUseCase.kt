package com.spacexrocketlaunches.module.launches.domain.usecase

import com.mollov.util.Result
import com.mollov.util.UseCase
import com.spacexrocketlaunches.module.launches.domain.LaunchesRepository
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import javax.inject.Inject

class GetRocketLaunchesUseCase @Inject constructor(
    private val launchesRepository: LaunchesRepository,
) : UseCase {

    suspend operator fun invoke(rocketId: String): Result<List<Launch>> {
        return launchesRepository.getLaunches(rocketId)
    }
}