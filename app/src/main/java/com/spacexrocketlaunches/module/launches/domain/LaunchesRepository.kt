package com.spacexrocketlaunches.module.launches.domain

import com.mollov.util.Result
import com.spacexrocketlaunches.module.launches.domain.model.Launch

interface LaunchesRepository {

    suspend fun getLaunches(rocketId: String): Result<List<Launch>>
}