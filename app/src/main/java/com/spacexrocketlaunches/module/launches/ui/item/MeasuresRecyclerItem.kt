package com.spacexrocketlaunches.module.launches.ui.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.spacexrocketlaunches.R
import com.spacexrocketlaunches.module.rockets.domain.model.Length
import com.spacexrocketlaunches.module.rockets.domain.model.Weight
import com.spacexrocketlaunches.module.rockets.ui.item.MeasureRecyclerItem
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter
import com.trading212.diverserecycleradapter.util.replaceItems
import java.math.RoundingMode

class MeasuresRecyclerItem(
    override val data: Data
) : DiverseRecyclerAdapter.RecyclerItem<MeasuresRecyclerItem.Data, MeasuresRecyclerItem.ViewHolder>() {

    override val type: Int = ItemTypes.MEASURES.type

    override val id: Long = type.toLong()

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.item_measures, parent, false))

    data class Data(
        val diameter: Length,
        val height: Length,
        val mass: Weight,
        val enginesCount: Int,
        val flights: Int?,
    )

    class ViewHolder(itemView: View) : DiverseRecyclerAdapter.ViewHolder<Data>(itemView) {

        private val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)

        private val adapter = DiverseRecyclerAdapter().also {
            it.setHasStableIds(true)
        }

        init {
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter
            recyclerView.layoutManager =
                LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        }

        override fun bindTo(data: Data) {

            val weight = (data.mass.lb / 1_000_000.0)
                .toBigDecimal()
                .setScale(2, RoundingMode.HALF_EVEN)

            adapter.replaceItems(
                listOf(
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_width,
                            "${data.diameter.feet} ft"
                        )
                    ),
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_height,
                            "${data.height.feet} ft"
                        )
                    ),
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_weight,
                            "${weight}M lb"
                        )
                    ),
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_engine,
                            "${data.enginesCount}"
                        )
                    ),
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_rocket,
                            if (data.flights != null) "${data.flights}" else "-",
                        )
                    ),
                )
            )
        }
    }
}