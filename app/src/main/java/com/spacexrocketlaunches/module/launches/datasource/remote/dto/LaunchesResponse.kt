package com.spacexrocketlaunches.module.launches.datasource.remote.dto

class LaunchesResponse(

    val docs: List<LaunchDto>,
)