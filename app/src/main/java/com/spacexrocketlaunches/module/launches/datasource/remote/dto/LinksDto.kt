package com.spacexrocketlaunches.module.launches.datasource.remote.dto

class LinksDto(

    val patch: PatchDto?,
)

class PatchDto(

    val small: String?,
)