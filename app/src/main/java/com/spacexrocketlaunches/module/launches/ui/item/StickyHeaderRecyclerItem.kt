package com.spacexrocketlaunches.module.launches.ui.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mollov.util.stickyheaders.StickyHeader
import com.spacexrocketlaunches.R
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter

class StickyHeaderRecyclerItem(
    override val data: String
) : DiverseRecyclerAdapter.RecyclerItem<String, StickyHeaderRecyclerItem.ViewHolder>() {

    override val type: Int = ItemTypes.STICKY_HEADER.type

    override val id: Long = data.hashCode().toLong()

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.item_sticky_header, parent, false))

    class ViewHolder(itemView: View) :
        DiverseRecyclerAdapter.ViewHolder<String>(itemView),
        StickyHeader {

        private val textView = findViewById<TextView>(R.id.textView)

        override val stickyId: Any
            get() = lastData ?: ""

        override fun bindTo(data: String) {
            textView.text = data
        }
    }
}