package com.spacexrocketlaunches.module.launches.datasource.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.spacexrocketlaunches.module.launches.datasource.local.entity.LaunchEntity

@Dao
abstract class LaunchesDao {

    @Query("SELECT * FROM LaunchEntity WHERE rocketId=:rocketId")
    abstract suspend fun getLaunches(rocketId: String): List<LaunchEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertLaunches(launches: List<LaunchEntity>)
}