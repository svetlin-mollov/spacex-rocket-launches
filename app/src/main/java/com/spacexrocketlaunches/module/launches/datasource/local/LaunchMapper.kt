package com.spacexrocketlaunches.module.launches.datasource.local

import com.spacexrocketlaunches.module.launches.datasource.local.entity.LaunchEntity
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LaunchMapper @Inject constructor() {

    fun mapToLaunchEntity(source: Launch): LaunchEntity {
        return LaunchEntity(
            id = source.id,
            name = source.name,
            details = source.details,
            launchDate = source.launchDate,
            success = source.success,
            missionPatchUrl = source.missionPatchUrl,
            rocketId = source.rocketId,
        )
    }

    fun mapFromLaunchEntity(source: LaunchEntity): Launch {
        return Launch(
            id = source.id,
            name = source.name,
            details = source.details,
            launchDate = source.launchDate,
            success = source.success,
            missionPatchUrl = source.missionPatchUrl,
            rocketId = source.rocketId,
        )
    }
}