package com.spacexrocketlaunches.module.launches.ui.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.spacexrocketlaunches.R
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter

class LaunchRecyclerItem(
    override val data: Launch
) : DiverseRecyclerAdapter.RecyclerItem<Launch, LaunchRecyclerItem.ViewHolder>() {

    override val type: Int = ItemTypes.LAUNCH.type

    override val id: Long = data.id.hashCode().toLong()

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.item_launch, parent, false))

    class ViewHolder(itemView: View) : DiverseRecyclerAdapter.ViewHolder<Launch>(itemView) {

        private val missionResultView = findViewById<View>(R.id.missionResultView)
        private val missionPatchImageView = findViewById<ImageView>(R.id.missionPatchImageView)
        private val nameTextView = findViewById<TextView>(R.id.nameTextView)
        private val launchDateTextView = findViewById<TextView>(R.id.launchDateTextView)
        private val missionResultTextView = findViewById<TextView>(R.id.missionResultTextView)
        private val detailsTextView = findViewById<TextView>(R.id.detailsTextView)

        override fun bindTo(data: Launch) {

            missionResultView.setBackgroundResource(
                if (data.success) R.drawable.bg_mission_success else R.drawable.bg_mission_failed
            )

            Glide
                .with(itemView.context)
                .load(data.missionPatchUrl)
                .centerCrop()
                .into(missionPatchImageView)

            nameTextView.text = data.name

            launchDateTextView.text = data.launchDate // TODO format date

            missionResultTextView.text = missionResultView.context.getText(
                if (data.success) R.string.mission_succeeded else R.string.mission_failed
            )

            detailsTextView.text = data.details
            detailsTextView.isVisible = data.details.isNotEmpty()
        }
    }
}