package com.spacexrocketlaunches.module.launches.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mollov.util.Result
import com.mollov.util.change
import com.spacexrocketlaunches.module.launches.domain.usecase.GetRocketLaunchesUseCase
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LaunchesViewModel @Inject constructor(
    private val getRocketLaunchesUseCase: GetRocketLaunchesUseCase,
) : ViewModel() {

    var rocket: Rocket? = null
        set(value) {
            if (field == value || value == null) return

            field = value

            viewModelScope.launch {

                _state.change {
                    it.copy(rocket = value)
                }

                loadRocketLaunches(value.id)
            }
        }

    private val _state = MutableStateFlow(LaunchesState(rocket))
    val state: Flow<LaunchesState> = _state

    fun refresh() {
        val rocketId = rocket?.id ?: return

        viewModelScope.launch {
            loadRocketLaunches(rocketId)
        }
    }

    private suspend fun loadRocketLaunches(rocketId: String) {
        _state.change {
            it.copy(
                error = null,
                isLoading = true,
            )
        }

        val result = getRocketLaunchesUseCase(rocketId)

        if (result is Result.Success) {
            _state.change {
                it.copy(
                    launches = result.data,
                    error = null,
                    isLoading = false,
                )
            }
        } else if (result is Result.Error) {
            _state.change {
                it.copy(
                    error = result.error,
                    isLoading = false,
                )
            }
        }
    }
}