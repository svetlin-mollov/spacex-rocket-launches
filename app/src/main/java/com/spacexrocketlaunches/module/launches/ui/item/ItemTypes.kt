package com.spacexrocketlaunches.module.launches.ui.item

enum class ItemTypes {

    MEASURES,
    DESCRIPTION,
    LAUNCHES_GRAPH,
    STICKY_HEADER,
    LAUNCH,
}

val ItemTypes.type: Int
    get() = ordinal