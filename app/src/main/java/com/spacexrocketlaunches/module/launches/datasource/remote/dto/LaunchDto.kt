package com.spacexrocketlaunches.module.launches.datasource.remote.dto

import com.google.gson.annotations.SerializedName

class LaunchDto(

    val id: String?,

    val name: String?,

    val details: String?,

    @SerializedName("date_utc")
    val launchDate: String?,

    val success: Boolean?,

    @SerializedName("rocket")
    val rocketId: String?,

    val links: LinksDto?,
)