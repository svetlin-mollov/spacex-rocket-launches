package com.spacexrocketlaunches.module.launches.datasource.local

import com.mollov.util.Result
import com.spacexrocketlaunches.module.launches.datasource.LaunchesDataSource
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LaunchesLocalDataSource @Inject constructor(
    private val launchesDao: LaunchesDao,
    private val mapper: LaunchMapper,
    private val coroutineDispatcher: CoroutineDispatcher,
) : LaunchesDataSource {

    override suspend fun getLaunches(rocketId: String): Result<List<Launch>> {
        return withContext(coroutineDispatcher) {

            try {
                val result = launchesDao
                    .getLaunches(rocketId)
                    .map { mapper.mapFromLaunchEntity(it) }

                Result.Success(result)
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }

    override suspend fun saveLaunches(launches: List<Launch>): Result<Unit> {
        return withContext(coroutineDispatcher) {

            try {
                launchesDao.insertLaunches(
                    launches.map { mapper.mapToLaunchEntity(it) }
                )

                Result.Success(Unit)
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }
}