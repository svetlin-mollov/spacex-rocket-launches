package com.spacexrocketlaunches.module.launches.datasource.remote

import com.spacexrocketlaunches.module.launches.datasource.remote.dto.LaunchDto
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LaunchMapper @Inject constructor() {

    fun mapToLaunchDto(source: Launch): LaunchDto {
        throw UnsupportedOperationException()
    }

    fun mapFromLaunchDto(source: LaunchDto): Launch {
        return Launch(
            id = source.id ?: "",
            name = source.name ?: "",
            details = source.details ?: "",
            launchDate = source.launchDate ?: "",
            success = source.success ?: false,
            missionPatchUrl = source.links?.patch?.small ?: "",
            rocketId = source.rocketId ?: "",
        )
    }
}