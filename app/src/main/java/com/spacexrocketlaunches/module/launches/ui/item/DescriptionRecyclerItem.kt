package com.spacexrocketlaunches.module.launches.ui.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.spacexrocketlaunches.R
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter

class DescriptionRecyclerItem(
    override val data: Data
) : DiverseRecyclerAdapter.RecyclerItem<DescriptionRecyclerItem.Data, DescriptionRecyclerItem.ViewHolder>() {

    override val type: Int = ItemTypes.DESCRIPTION.type

    override val id: Long = data.hashCode().toLong()

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.item_description, parent, false))

    data class Data(
        val key: String,
        val value: String,
    )

    class ViewHolder(itemView: View) : DiverseRecyclerAdapter.ViewHolder<Data>(itemView) {

        private val keyTextView = findViewById<TextView>(R.id.keyTextView)
        private val valueTextView = findViewById<TextView>(R.id.valueTextView)

        override fun bindTo(data: Data) {
            keyTextView.text = data.key
            valueTextView.text = data.value
        }
    }
}