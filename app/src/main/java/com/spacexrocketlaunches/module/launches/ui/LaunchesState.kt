package com.spacexrocketlaunches.module.launches.ui

import com.spacexrocketlaunches.module.launches.domain.model.Launch
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket

data class LaunchesState(
    val rocket: Rocket?,
    val launches: List<Launch>? = null,
    val error: Throwable? = null,
    val isLoading: Boolean = false,
)