package com.spacexrocketlaunches.module.launches.di

import com.spacexrocketlaunches.db.RocketsDb
import com.spacexrocketlaunches.module.launches.datasource.CachingLaunchesDataSource
import com.spacexrocketlaunches.module.launches.datasource.LaunchesDataSource
import com.spacexrocketlaunches.module.launches.datasource.local.LaunchesDao
import com.spacexrocketlaunches.module.launches.datasource.local.LaunchesLocalDataSource
import com.spacexrocketlaunches.module.launches.datasource.remote.LaunchesRemoteDataSource
import com.spacexrocketlaunches.module.launches.datasource.remote.LaunchesRetrofitService
import com.spacexrocketlaunches.module.launches.domain.LaunchesRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LaunchesProviderModule {

    @Singleton
    @Provides
    fun provideLaunchesRetrofitService(retrofit: Retrofit): LaunchesRetrofitService {
        return retrofit.create(LaunchesRetrofitService::class.java)
    }

    @Singleton
    @Provides
    fun provideLaunchesDao(database: RocketsDb): LaunchesDao {
        return database.launchesDao()
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class LaunchesBinderModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class RemoteLaunchesDataSource

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LocalLaunchesDataSource

    @RemoteLaunchesDataSource
    @Singleton
    @Binds
    abstract fun bindLaunchesRemoteDataSource(
        launchesRemoteDataSource: LaunchesRemoteDataSource
    ): LaunchesDataSource

    @LocalLaunchesDataSource
    @Singleton
    @Binds
    abstract fun bindLaunchesLocalDataSource(
        launchesLocalDataSource: LaunchesLocalDataSource
    ): LaunchesDataSource

    @Singleton
    @Binds
    abstract fun bindLaunchesRepository(
        cachingLaunchesDataSource: CachingLaunchesDataSource
    ): LaunchesRepository
}