package com.spacexrocketlaunches.module.launches.ui.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.spacexrocketlaunches.R
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import com.spacexrocketlaunches.module.launches.ui.chart.ChartView
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter

class LaunchesGraphRecyclerItem(
    override val data: Map<String, List<Launch>>
) : DiverseRecyclerAdapter.RecyclerItem<Map<String, List<Launch>>, LaunchesGraphRecyclerItem.ViewHolder>() {

    override val type: Int = ItemTypes.LAUNCHES_GRAPH.type

    override val id: Long = type.toLong()

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.item_launches_graph, parent, false))

    class ViewHolder(itemView: View) :
        DiverseRecyclerAdapter.ViewHolder<Map<String, List<Launch>>>(itemView) {

        private val chartView = findViewById<ChartView>(R.id.chartView)

        override fun bindTo(data: Map<String, List<Launch>>) {
            chartView.launchesPerYear = data.mapValues { it.value.size }
        }
    }
}