package com.spacexrocketlaunches.module.launches.datasource.remote.dto

class LaunchesRequest(

    val query: Query,
)

class Query(

    val rocket: String,
)
