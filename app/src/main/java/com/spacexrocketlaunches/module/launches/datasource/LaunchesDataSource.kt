package com.spacexrocketlaunches.module.launches.datasource

import com.mollov.util.Result
import com.spacexrocketlaunches.module.launches.domain.LaunchesRepository
import com.spacexrocketlaunches.module.launches.domain.model.Launch

interface LaunchesDataSource : LaunchesRepository {

    suspend fun saveLaunches(launches: List<Launch>): Result<Unit>
}