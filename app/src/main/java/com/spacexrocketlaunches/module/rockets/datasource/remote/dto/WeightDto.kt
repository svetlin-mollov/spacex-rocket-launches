package com.spacexrocketlaunches.module.rockets.datasource.remote.dto

class WeightDto(

    val kg: Double?,

    val lb: Double?,
)