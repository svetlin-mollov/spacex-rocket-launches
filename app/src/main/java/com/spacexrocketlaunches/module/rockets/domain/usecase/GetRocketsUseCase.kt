package com.spacexrocketlaunches.module.rockets.domain.usecase

import com.mollov.util.Result
import com.mollov.util.UseCase
import com.spacexrocketlaunches.module.rockets.domain.RocketsRepository
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import javax.inject.Inject

class GetRocketsUseCase @Inject constructor(
    private val rocketsRepository: RocketsRepository,
) : UseCase {

    suspend operator fun invoke(): Result<List<Rocket>> {
        return rocketsRepository.getRockets()
    }
}