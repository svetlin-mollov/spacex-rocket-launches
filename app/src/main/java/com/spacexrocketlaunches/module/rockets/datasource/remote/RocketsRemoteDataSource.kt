package com.spacexrocketlaunches.module.rockets.datasource.remote

import com.mollov.util.Result
import com.spacexrocketlaunches.http.ExceptionResolver
import com.spacexrocketlaunches.module.rockets.datasource.RocketsDataSource
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RocketsRemoteDataSource @Inject constructor(
    private val service: RocketsRetrofitService,
    private val mapper: RocketsMapper,
    private val exceptionResolver: ExceptionResolver,
    private val coroutineDispatcher: CoroutineDispatcher,
) : RocketsDataSource {

    override suspend fun saveRockets(rockets: List<Rocket>): Result<Unit> {

        // Just return success, because the remote storage is not available
        return Result.Success(Unit)
    }

    override suspend fun getRocket(rocketId: String): Result<Rocket> {
        return Result.Error(UnsupportedOperationException())
    }

    override suspend fun getRockets(): Result<List<Rocket>> {
        return withContext(coroutineDispatcher) {

            try {
                val response = service.getRockets()

                val body = response.body()
                if (response.isSuccessful && body != null) {
                    Result.Success(body.map { mapper.mapFromRocketDto(it) })
                } else {
                    Result.Error(
                        exceptionResolver.resolveError(response)
                    )
                }
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }
}