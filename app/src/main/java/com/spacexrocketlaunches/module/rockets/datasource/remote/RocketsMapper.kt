package com.spacexrocketlaunches.module.rockets.datasource.remote

import com.spacexrocketlaunches.module.rockets.datasource.remote.dto.RocketDto
import com.spacexrocketlaunches.module.rockets.domain.model.Length
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import com.spacexrocketlaunches.module.rockets.domain.model.Weight
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RocketsMapper @Inject constructor() {

    fun mapToRocketDto(source: Rocket): RocketDto {
        throw UnsupportedOperationException()
    }

    fun mapFromRocketDto(source: RocketDto): Rocket {
        return Rocket(
            id = source.id ?: "",
            name = source.name ?: "",
            country = source.country ?: "",
            description = source.description ?: "",
            wikipedia = source.wikipedia ?: "",
            height = Length(source.height?.meters ?: 0.0, source.height?.feet ?: 0.0),
            diameter = Length(source.diameter?.meters ?: 0.0, source.diameter?.feet ?: 0.0),
            mass = Weight(source.mass?.kg ?: 0.0, source.mass?.lb ?: 0.0),
            enginesCount = source.engines?.number ?: 0,
            firstFlight = source.firstFlight ?: "",
            isActive = source.isActive ?: false,
            image = source.flickrImages?.firstOrNull() ?: "",
        )
    }
}