package com.spacexrocketlaunches.module.rockets.datasource.remote.dto

import com.google.gson.annotations.SerializedName

class RocketDto(

    val id: String?,

    val name: String?,

    val country: String?,

    val description: String?,

    val wikipedia: String?,

    val height: LengthDto?,

    val diameter: LengthDto?,

    val mass: WeightDto?,

    val engines: EnginesDto?,

    @SerializedName("first_flight")
    val firstFlight: String?,

    @SerializedName("active")
    val isActive: Boolean?,

    @SerializedName("flickr_images")
    val flickrImages: List<String>?,
)