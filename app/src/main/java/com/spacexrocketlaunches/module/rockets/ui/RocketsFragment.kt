package com.spacexrocketlaunches.module.rockets.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mollov.util.whileIn
import com.spacexrocketlaunches.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@FlowPreview
@AndroidEntryPoint
class RocketsFragment : Fragment() {

    private val viewModel: RocketsViewModel by viewModels()

    private lateinit var listLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Create the LayoutManager here to preserve the scroll position when the Fragment's view is
        // re-created on return from the back stack
        listLayoutManager = LinearLayoutManager(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_rockets, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val rocketsView = view.findViewById<RocketsView>(R.id.rocketsView).apply {
            layoutManager = listLayoutManager
            onRefresh = { viewModel.refresh() }
            onFilter = { viewModel.filterRockets(it) }
            onRocketSelected = { viewModel.selectRocket(it) }
        }

        viewLifecycleOwner.whileIn(Lifecycle.State.STARTED) {

            // Consume state changes in a new coroutine
            launch {
                viewModel.state.collect {
                    rocketsView.state = it
                }
            }

            // Consume events in a new coroutine
            launch {
                viewModel.events.collect { event ->
                    if (event is RocketsEvent.ShowRocketDetails) {
                        findNavController().navigate(
                            RocketsFragmentDirections.actionRocketsFragmentToLaunchesFragment(
                                event.rocket
                            ),
                            FragmentNavigatorExtras(
                                rocketsView.findViewWithTag<View>(event.rocket.image) to event.rocket.image
                            ),
                        )
                    }
                }
            }
        }
    }
}