package com.spacexrocketlaunches.module.rockets.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rocket(
    val id: String,
    val name: String,
    val country: String,
    val description: String,
    val wikipedia: String,
    val height: Length,
    val diameter: Length,
    val mass: Weight,
    val enginesCount: Int,
    val firstFlight: String,
    val isActive: Boolean,
    val image: String,
) : Parcelable
