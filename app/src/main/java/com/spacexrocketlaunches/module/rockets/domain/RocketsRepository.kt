package com.spacexrocketlaunches.module.rockets.domain

import com.mollov.util.Result
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket

interface RocketsRepository {

    suspend fun getRocket(rocketId: String): Result<Rocket>

    suspend fun getRockets(): Result<List<Rocket>>
}