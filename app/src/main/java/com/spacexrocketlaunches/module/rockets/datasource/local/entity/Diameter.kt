package com.spacexrocketlaunches.module.rockets.datasource.local.entity

class Diameter(

    val diameterMeters: Double,

    val diameterFeet: Double,
)