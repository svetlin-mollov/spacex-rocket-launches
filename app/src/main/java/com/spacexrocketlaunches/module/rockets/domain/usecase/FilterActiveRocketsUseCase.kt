package com.spacexrocketlaunches.module.rockets.domain.usecase

import com.mollov.util.UseCase
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import javax.inject.Inject

class FilterActiveRocketsUseCase @Inject constructor() : UseCase {

    operator fun invoke(rockets: List<Rocket>?, showOnlyActive: Boolean?): List<Rocket> {

        if (rockets.isNullOrEmpty()) return emptyList()

        if (showOnlyActive == null || showOnlyActive == false) return rockets

        return rockets.filter { it.isActive }
    }
}