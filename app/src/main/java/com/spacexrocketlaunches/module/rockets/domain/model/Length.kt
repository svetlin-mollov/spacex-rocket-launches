package com.spacexrocketlaunches.module.rockets.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Length(
    val meters: Double,
    val feet: Double,
) : Parcelable