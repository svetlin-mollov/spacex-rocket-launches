package com.spacexrocketlaunches.module.rockets.datasource.local.entity

class Mass(

    val massKg: Double,

    val massLb: Double,
)