package com.spacexrocketlaunches.module.rockets.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mollov.util.Result
import com.mollov.util.change
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import com.spacexrocketlaunches.module.rockets.domain.usecase.FilterActiveRocketsUseCase
import com.spacexrocketlaunches.module.rockets.domain.usecase.GetRocketsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@FlowPreview
@HiltViewModel
class RocketsViewModel @Inject constructor(
    private val getRocketsUseCase: GetRocketsUseCase,
    private val filterActiveRocketsUseCase: FilterActiveRocketsUseCase,
) : ViewModel() {

    private val _activeFilterFlow = MutableStateFlow<Boolean?>(null)

    private val _state = MutableStateFlow(RocketsState())

    val state: Flow<RocketsState> = _state
        .combine(_activeFilterFlow) { state, filter ->
            state.copy(
                activeFilter = filter,
                visibleRockets = filterActiveRocketsUseCase(state.rockets, filter),
            )
        }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = RocketsState(isLoading = true),
        )

    private val _events = MutableSharedFlow<RocketsEvent>()
    val events: Flow<RocketsEvent> = _events

    init {
        loadRockets()
    }

    fun filterRockets(active: Boolean?) {
        viewModelScope.launch {
            _activeFilterFlow.emit(active)
        }
    }

    fun refresh() {
        if (_state.value.isLoading) return

        loadRockets()
    }

    fun selectRocket(rocket: Rocket) {
        viewModelScope.launch {
            _events.emit(RocketsEvent.ShowRocketDetails(rocket))
        }
    }

    private fun loadRockets() {
        viewModelScope.launch {

            _state.change {
                it.copy(
                    error = null,
                    isLoading = true,
                )
            }

            val result = getRocketsUseCase()

            if (result is Result.Success) {
                _state.change {
                    it.copy(
                        rockets = result.data,
                        error = null,
                        isLoading = false,
                    )
                }
            } else if (result is Result.Error) {
                _state.change {
                    it.copy(
                        error = result.error,
                        isLoading = false,
                    )
                }
            }
        }
    }
}