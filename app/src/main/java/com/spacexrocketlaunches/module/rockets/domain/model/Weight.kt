package com.spacexrocketlaunches.module.rockets.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Weight(
    val kg: Double,
    val lb: Double,
) : Parcelable