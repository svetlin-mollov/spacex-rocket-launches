package com.spacexrocketlaunches.module.rockets.datasource.local.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class RocketEntity(

    @PrimaryKey
    val id: String,

    val name: String,

    val country: String,

    val description: String,

    val wikipedia: String,

    @Embedded
    val height: Height,

    @Embedded
    val diameter: Diameter,

    @Embedded
    val mass: Mass,

    val enginesCount: Int,

    val firstFlight: String,

    val isActive: Boolean,

    val image: String,
)