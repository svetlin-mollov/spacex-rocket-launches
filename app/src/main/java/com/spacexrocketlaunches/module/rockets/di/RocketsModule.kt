package com.spacexrocketlaunches.module.rockets.di

import com.spacexrocketlaunches.db.RocketsDb
import com.spacexrocketlaunches.module.rockets.datasource.RocketsCachingDataSource
import com.spacexrocketlaunches.module.rockets.datasource.RocketsDataSource
import com.spacexrocketlaunches.module.rockets.datasource.local.RocketsDao
import com.spacexrocketlaunches.module.rockets.datasource.local.RocketsLocalDataSource
import com.spacexrocketlaunches.module.rockets.datasource.remote.RocketsRemoteDataSource
import com.spacexrocketlaunches.module.rockets.datasource.remote.RocketsRetrofitService
import com.spacexrocketlaunches.module.rockets.domain.RocketsRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RocketsProviderModule {

    @Singleton
    @Provides
    fun provideRocketsRetrofitService(retrofit: Retrofit): RocketsRetrofitService {
        return retrofit.create(RocketsRetrofitService::class.java)
    }

    @Singleton
    @Provides
    fun provideRocketsDao(database: RocketsDb): RocketsDao {
        return database.rocketsDao()
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class RocketsBinderModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class RemoteRocketsDataSource

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LocalRocketsDataSource

    @RemoteRocketsDataSource
    @Singleton
    @Binds
    abstract fun bindRocketsRemoteDataSource(
        rocketsRemoteDataSource: RocketsRemoteDataSource
    ): RocketsDataSource

    @LocalRocketsDataSource
    @Singleton
    @Binds
    abstract fun bindRocketsLocalDataSource(
        rocketsLocalDataSource: RocketsLocalDataSource
    ): RocketsDataSource

    @Singleton
    @Binds
    abstract fun bindRocketsRepository(
        rocketsCachingDataSource: RocketsCachingDataSource
    ): RocketsRepository
}