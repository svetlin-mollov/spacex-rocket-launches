package com.spacexrocketlaunches.module.rockets.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.mollov.util.Diff
import com.mollov.util.dp
import com.mollov.util.view.StatefulView
import com.spacexrocketlaunches.R
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import com.spacexrocketlaunches.module.rockets.ui.item.RocketRecyclerItem
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter
import com.trading212.diverserecycleradapter.util.onItemClicked
import com.trading212.diverserecycleradapter.util.replaceItems
import kotlinx.android.synthetic.main.view_rockets.view.*

class RocketsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : StatefulView<RocketsState>(context, attrs, defStyleAttr) {

    var onRefresh: () -> Unit = {}
    var onFilter: (active: Boolean?) -> Unit = {}
    var onRocketSelected: (rocket: Rocket) -> Unit = {}

    var layoutManager: LinearLayoutManager = LinearLayoutManager(context)

    private val adapter = DiverseRecyclerAdapter().also {
        it.setHasStableIds(true)
    }

    override fun createView(inflater: LayoutInflater, parent: ViewGroup): View =
        inflater.inflate(R.layout.view_rockets, parent, false)

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)

        adapter.onItemClicked { _, position ->
            if (adapter.getItemViewType(position) == RocketRecyclerItem.TYPE) {
                val rocket = adapter.getItem<RocketRecyclerItem>(position).data
                onRocketSelected.invoke(rocket)
            }
        }

        activeFilterSwitch.setOnCheckedChangeListener { _, isChecked ->
            onFilter(isChecked)
        }

        swipeToRefreshLayout.setProgressViewOffset(true, 50.dp, 80.dp)
        swipeToRefreshLayout.setOnRefreshListener {
            onRefresh()
        }
    }

    override fun render(state: RocketsState, diff: Diff<RocketsState>) {
        if (diff.byList { it.visibleRockets }) {
            adapter.replaceItems(
                state.visibleRockets.map { RocketRecyclerItem(it) }
            )

            noResultsTextView.isVisible =
                state.visibleRockets.isEmpty() && state.rockets.isNotEmpty()

            if (diff.by { it.activeFilter } && state.activeFilter != null) {
                recyclerView.post {
                    recyclerView.smoothScrollToPosition(0)
                }
            }
        }

        if (diff.by { it.isLoading } || diff.byList { it.rockets }) {
            noItemsTextView.isVisible = state.rockets.isEmpty() && !state.isLoading
        }

        if (diff.by { it.isLoading }) {
            progressIndicator.isVisible = state.isLoading && state.rockets.isEmpty()

            val isRefreshing = state.isLoading && state.rockets.isNotEmpty()
            swipeToRefreshLayout.post {
                swipeToRefreshLayout.isRefreshing = isRefreshing
            }
        }

        if (diff.by { it.error }) {
            errorTextView.isVisible = state.error != null
            errorTextView.text = state.error?.message
        }
    }
}