package com.spacexrocketlaunches.module.rockets.datasource.remote.dto

class LengthDto(

    val meters: Double?,

    val feet: Double?,
)