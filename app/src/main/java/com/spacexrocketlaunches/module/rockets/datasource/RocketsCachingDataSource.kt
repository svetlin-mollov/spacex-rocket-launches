package com.spacexrocketlaunches.module.rockets.datasource

import com.mollov.util.Result
import com.spacexrocketlaunches.module.rockets.di.RocketsBinderModule
import com.spacexrocketlaunches.module.rockets.domain.RocketsRepository
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import javax.inject.Inject

class RocketsCachingDataSource @Inject constructor(
    @RocketsBinderModule.RemoteRocketsDataSource
    private val rocketsRemoteDataSource: RocketsDataSource,

    @RocketsBinderModule.LocalRocketsDataSource
    private val rocketsLocalDataSource: RocketsDataSource,
) : RocketsRepository {

    override suspend fun getRocket(rocketId: String): Result<Rocket> {
        return rocketsLocalDataSource.getRocket(rocketId)
    }

    override suspend fun getRockets(): Result<List<Rocket>> {

        val remoteResult = rocketsRemoteDataSource.getRockets()

        return if (remoteResult is Result.Success) {
            rocketsLocalDataSource.saveRockets(remoteResult.data)

            remoteResult
        } else {
            rocketsLocalDataSource.getRockets()
        }
    }
}