package com.spacexrocketlaunches.module.rockets.datasource.local

import androidx.room.*
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.RocketEntity

@Dao
abstract class RocketsDao {

    @Query("SELECT * FROM RocketEntity")
    abstract suspend fun getRockets(): List<RocketEntity>

    @Query("SELECT * FROM RocketEntity WHERE id=:rocketId")
    abstract suspend fun getRocket(rocketId: String): RocketEntity

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertRockets(list: List<RocketEntity>)
}