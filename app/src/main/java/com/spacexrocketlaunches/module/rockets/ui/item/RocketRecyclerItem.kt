package com.spacexrocketlaunches.module.rockets.ui.item

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.spacexrocketlaunches.R
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter
import com.trading212.diverserecycleradapter.util.replaceItems
import java.math.RoundingMode

class RocketRecyclerItem(
    override val data: Rocket
) : DiverseRecyclerAdapter.RecyclerItem<Rocket, RocketRecyclerItem.ViewHolder>() {

    override val id: Long = data.id.hashCode().toLong()

    override val type: Int = TYPE

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.item_rocket, parent, false))

    class ViewHolder(itemView: View) : DiverseRecyclerAdapter.ViewHolder<Rocket>(itemView) {

        private val imageView = findViewById<ImageView>(R.id.imageView)
        private val titleTextView = findViewById<TextView>(R.id.titleTextView)
        private val countryTextView = findViewById<TextView>(R.id.countryTextView)
        private val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)

        private val adapter = DiverseRecyclerAdapter().also {
            it.setHasStableIds(true)
        }

        init {
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter
            recyclerView.layoutManager =
                LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        }

        @SuppressLint("SetTextI18n")
        override fun bindTo(data: Rocket) {

            Glide
                .with(itemView)
                .load(data.image)
                .placeholder(R.drawable.ic_rocket_placeholder)
                .centerCrop()
                .into(imageView)

            imageView.tag = data.image
            imageView.transitionName = data.image

            titleTextView.text = data.name
            countryTextView.text = data.country

            val weight = (data.mass.lb / 1_000_000.0)
                .toBigDecimal()
                .setScale(2, RoundingMode.HALF_EVEN)

            adapter.replaceItems(
                listOf(
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_width,
                            "${data.diameter.feet} ft"
                        )
                    ),
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_height,
                            "${data.height.feet} ft"
                        )
                    ),
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_weight,
                            "${weight}M lb"
                        )
                    ),
                    MeasureRecyclerItem(
                        MeasureRecyclerItem.Data(
                            R.drawable.ic_engine,
                            "${data.enginesCount}"
                        )
                    ),
                )
            )
        }
    }

    companion object {
        const val TYPE = 143
    }
}