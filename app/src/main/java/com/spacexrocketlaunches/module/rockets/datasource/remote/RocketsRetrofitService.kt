package com.spacexrocketlaunches.module.rockets.datasource.remote

import com.spacexrocketlaunches.module.rockets.datasource.remote.dto.RocketDto
import retrofit2.Response
import retrofit2.http.GET

interface RocketsRetrofitService {

    @GET("/v4/rockets")
    suspend fun getRockets(): Response<List<RocketDto>>
}