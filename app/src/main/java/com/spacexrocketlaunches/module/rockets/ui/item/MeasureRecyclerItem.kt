package com.spacexrocketlaunches.module.rockets.ui.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.spacexrocketlaunches.R
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter

class MeasureRecyclerItem(
    override val data: Data,
) : DiverseRecyclerAdapter.RecyclerItem<MeasureRecyclerItem.Data, MeasureRecyclerItem.ViewHolder>() {

    override val id: Long = data.hashCode().toLong()

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.item_measure, parent, false))

    data class Data(
        val iconRes: Int,
        val measure: String,
    )

    class ViewHolder(itemView: View) : DiverseRecyclerAdapter.ViewHolder<Data>(itemView) {

        private val measureTextView = findViewById<TextView>(R.id.measureTextView)

        override fun bindTo(data: Data) {
            measureTextView.text = data.measure
            measureTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(data.iconRes, 0, 0, 0)
        }
    }
}