package com.spacexrocketlaunches.module.rockets.datasource.local

import com.mollov.util.Result
import com.spacexrocketlaunches.module.rockets.datasource.RocketsDataSource
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RocketsLocalDataSource @Inject constructor(
    private val rocketsDao: RocketsDao,
    private val mapper: RocketsMapper,
    private val coroutineDispatcher: CoroutineDispatcher,
) : RocketsDataSource {

    override suspend fun saveRockets(rockets: List<Rocket>): Result<Unit> {
        return withContext(coroutineDispatcher) {

            try {
                rocketsDao.insertRockets(rockets.map { mapper.mapToRocketEntity(it) })

                Result.Success(Unit)
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }

    override suspend fun getRocket(rocketId: String): Result<Rocket> {
        return withContext(coroutineDispatcher) {

            try {
                val result = rocketsDao
                    .getRocket(rocketId)
                    .let { mapper.mapFromRocketEntity(it) }

                Result.Success(result)
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }

    override suspend fun getRockets(): Result<List<Rocket>> {
        return withContext(coroutineDispatcher) {

            try {
                val result = rocketsDao
                    .getRockets()
                    .map { mapper.mapFromRocketEntity(it) }

                Result.Success(result)
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }
}