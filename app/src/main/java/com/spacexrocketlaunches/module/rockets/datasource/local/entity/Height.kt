package com.spacexrocketlaunches.module.rockets.datasource.local.entity

class Height(

    val heightMeters: Double,

    val heightFeet: Double,
)