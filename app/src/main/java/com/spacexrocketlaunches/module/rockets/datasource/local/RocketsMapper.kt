package com.spacexrocketlaunches.module.rockets.datasource.local

import com.spacexrocketlaunches.module.rockets.datasource.local.entity.Diameter
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.Height
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.Mass
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.RocketEntity
import com.spacexrocketlaunches.module.rockets.domain.model.Length
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import com.spacexrocketlaunches.module.rockets.domain.model.Weight
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RocketsMapper @Inject constructor() {

    fun mapToRocketEntity(source: Rocket): RocketEntity {
        return RocketEntity(
            id = source.id,
            name = source.name,
            country = source.country,
            description = source.description,
            wikipedia = source.wikipedia,
            height = Height(source.height.meters, source.height.feet),
            diameter = Diameter(source.diameter.meters, source.diameter.feet),
            mass = Mass(source.mass.kg, source.mass.lb),
            enginesCount = source.enginesCount,
            firstFlight = source.firstFlight,
            isActive = source.isActive,
            image = source.image,
        )
    }

    fun mapFromRocketEntity(source: RocketEntity): Rocket {
        return Rocket(
            id = source.id,
            name = source.name,
            country = source.country,
            description = source.description,
            wikipedia = source.wikipedia,
            height = Length(source.height.heightMeters, source.height.heightFeet),
            diameter = Length(source.diameter.diameterMeters, source.diameter.diameterFeet),
            mass = Weight(source.mass.massKg, source.mass.massLb),
            enginesCount = source.enginesCount,
            firstFlight = source.firstFlight,
            isActive = source.isActive,
            image = source.image,
        )
    }
}