package com.spacexrocketlaunches.module.rockets.datasource

import com.mollov.util.Result
import com.spacexrocketlaunches.module.rockets.domain.RocketsRepository
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket

interface RocketsDataSource : RocketsRepository {

    suspend fun saveRockets(rockets: List<Rocket>): Result<Unit>
}