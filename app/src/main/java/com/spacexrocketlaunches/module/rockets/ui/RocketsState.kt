package com.spacexrocketlaunches.module.rockets.ui

import com.spacexrocketlaunches.module.rockets.domain.model.Rocket

data class RocketsState(
    val rockets: List<Rocket> = emptyList(),
    val visibleRockets: List<Rocket> = emptyList(),
    val activeFilter: Boolean? = null,
    val error: Throwable? = null,
    val isLoading: Boolean = false,
)