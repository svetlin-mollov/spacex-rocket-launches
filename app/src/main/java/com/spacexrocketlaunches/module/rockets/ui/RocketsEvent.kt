package com.spacexrocketlaunches.module.rockets.ui

import com.spacexrocketlaunches.module.rockets.domain.model.Rocket

sealed class RocketsEvent {

    class ShowRocketDetails(val rocket: Rocket) : RocketsEvent()
}
