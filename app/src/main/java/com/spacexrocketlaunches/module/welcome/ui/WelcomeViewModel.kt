package com.spacexrocketlaunches.module.welcome.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mollov.util.change
import com.spacexrocketlaunches.module.welcome.domain.usecase.MarkWelcomeAsShownUseCase
import com.spacexrocketlaunches.module.welcome.domain.usecase.ShouldShowWelcomeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WelcomeViewModel @Inject constructor(
    shouldShowWelcomeUseCase: ShouldShowWelcomeUseCase,
    private val markWelcomeAsShownUseCase: MarkWelcomeAsShownUseCase,
) : ViewModel() {

    private val _state = MutableStateFlow(WelcomeState(shouldShow = true))
    val state: Flow<WelcomeState> = _state

    private val _events = MutableSharedFlow<WelcomeEvent>()
    val events: Flow<WelcomeEvent> = _events

    init {
        if (!shouldShowWelcomeUseCase()) {
            viewModelScope.launch {
                _state.change {
                    it.copy(shouldShow = false)
                }
            }
        }
    }

    fun changeShowOnNextStartCheck(checked: Boolean) {
        viewModelScope.launch {
            _state.change {
                it.copy(showOnNextStartChecked = checked)
            }
        }
    }

    fun launch() {
        viewModelScope.launch {

            markWelcomeAsShownUseCase(_state.value.showOnNextStartChecked)

            _events.emit(WelcomeEvent.NavigateToRockets)
        }
    }
}