package com.spacexrocketlaunches.module.welcome.di

import com.spacexrocketlaunches.module.welcome.datasource.WelcomeSharedPreferencesDataSource
import com.spacexrocketlaunches.module.welcome.domain.WelcomeRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class WelcomeBinderModule {

    @Singleton
    @Binds
    abstract fun bindWelcomeRepository(
        welcomeSharedPreferencesDataSource: WelcomeSharedPreferencesDataSource
    ): WelcomeRepository
}