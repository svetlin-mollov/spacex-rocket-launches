package com.spacexrocketlaunches.module.welcome.domain.usecase

import com.mollov.util.UseCase
import com.spacexrocketlaunches.module.welcome.domain.WelcomeRepository
import javax.inject.Inject

class MarkWelcomeAsShownUseCase @Inject constructor(
    private val welcomeRepository: WelcomeRepository,
) : UseCase {

    operator fun invoke(showOnNextStartChecked: Boolean) {

        if (showOnNextStartChecked) return

        welcomeRepository.isWelcomeShown = true
    }
}