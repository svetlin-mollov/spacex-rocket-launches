package com.spacexrocketlaunches.module.welcome.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import com.mollov.util.Diff
import com.mollov.util.dpF
import com.mollov.util.view.StatefulView
import com.spacexrocketlaunches.R
import kotlinx.android.synthetic.main.view_welcome.view.*

class WelcomeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : StatefulView<WelcomeState>(context, attrs, defStyleAttr) {

    var onShownOnNextTimeCheckChanged: (isChecked: Boolean) -> Unit = {}

    var onLaunch: () -> Unit = {}

    private var isAnimating = false

    override fun createView(inflater: LayoutInflater, parent: ViewGroup): View =
        inflater.inflate(R.layout.view_welcome, parent, false)

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)

        showOnNexStartSwitch.setOnCheckedChangeListener { _, isChecked ->
            onShownOnNextTimeCheckChanged.invoke(isChecked)
        }

        launchButton.setOnClickListener {
            playLaunchAnimation()
        }
    }

    override fun render(state: WelcomeState, diff: Diff<WelcomeState>) {
        if (diff.by { it.showOnNextStartChecked }) {
            showOnNexStartSwitch.isChecked = state.showOnNextStartChecked
        }
    }

    private fun playLaunchAnimation() {
        if (isAnimating) return
        isAnimating = true

        rocketImageView.animate()
            .translationYBy((launchButton.top - rocketImageView.bottom).toFloat())
            .setInterpolator(INTERPOLATOR)
            .setDuration(ANIM_DURATION)
            .withEndAction {
                postDelayed({ onLaunch() }, ANIM_DURATION)
            }

        welcomeTextView.animate()
            .alpha(0f)
            .translationYBy(ANIM_LAUNCH_TRANSLATION_Y)
            .setInterpolator(INTERPOLATOR)
            .duration = ANIM_DURATION

        showOnNexStartSwitch.animate()
            .alpha(0f)
            .translationYBy(ANIM_LAUNCH_TRANSLATION_Y)
            .setInterpolator(INTERPOLATOR)
            .duration = ANIM_DURATION

        launchButton.animate()
            .alpha(0f)
            .setInterpolator(INTERPOLATOR)
            .duration = ANIM_DURATION
    }

    companion object {

        private const val ANIM_DURATION = 400L
        private val ANIM_LAUNCH_TRANSLATION_Y = 50.dpF
        private val INTERPOLATOR = DecelerateInterpolator()
    }
}