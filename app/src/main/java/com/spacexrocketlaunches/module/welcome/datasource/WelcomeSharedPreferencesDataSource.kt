package com.spacexrocketlaunches.module.welcome.datasource

import android.content.SharedPreferences
import com.spacexrocketlaunches.module.welcome.domain.WelcomeRepository
import javax.inject.Inject

class WelcomeSharedPreferencesDataSource @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) : WelcomeRepository {

    override var isWelcomeShown: Boolean
        get() = sharedPreferences.getBoolean(PREF_WELCOME_SHOWN, false)
        set(value) {
            sharedPreferences.edit().putBoolean(PREF_WELCOME_SHOWN, value).apply()
        }

    companion object {

        private const val PREF_WELCOME_SHOWN = "welcomeScreenShown"
    }
}