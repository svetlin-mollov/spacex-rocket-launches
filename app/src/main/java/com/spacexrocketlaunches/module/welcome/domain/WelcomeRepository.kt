package com.spacexrocketlaunches.module.welcome.domain

interface WelcomeRepository {

    var isWelcomeShown: Boolean
}