package com.spacexrocketlaunches.module.welcome.ui

sealed class WelcomeEvent {

    object NavigateToRockets : WelcomeEvent()
}
