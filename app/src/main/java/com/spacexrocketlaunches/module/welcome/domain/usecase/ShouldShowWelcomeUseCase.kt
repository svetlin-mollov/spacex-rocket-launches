package com.spacexrocketlaunches.module.welcome.domain.usecase

import com.mollov.util.UseCase
import com.spacexrocketlaunches.module.welcome.domain.WelcomeRepository
import javax.inject.Inject

class ShouldShowWelcomeUseCase @Inject constructor(
    private val welcomeRepository: WelcomeRepository,
) : UseCase {

    operator fun invoke(): Boolean {
        return !welcomeRepository.isWelcomeShown
    }
}