package com.spacexrocketlaunches.module.welcome.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.mollov.util.whileIn
import com.spacexrocketlaunches.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WelcomeFragment : Fragment() {

    private val viewModel: WelcomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_welcome, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<WelcomeView>(R.id.welcomeView).also { welcomeView ->
            welcomeView.onShownOnNextTimeCheckChanged = { viewModel.changeShowOnNextStartCheck(it) }
            welcomeView.onLaunch = { viewModel.launch() }
        }

        viewLifecycleOwner.whileIn(Lifecycle.State.STARTED) {

            // Consume state changes in a new coroutine
            launch {
                viewModel.state.collect {
                    if (!it.shouldShow) navigateToRockets(false)
                }
            }

            // Consume events in a new coroutine
            launch {
                viewModel.events.collect { event ->
                    if (event is WelcomeEvent.NavigateToRockets) {
                        navigateToRockets(true)
                    }
                }
            }
        }
    }

    private fun navigateToRockets(animate: Boolean) {
        findNavController().navigate(
            WelcomeFragmentDirections.actionWelcomeFragmentToRocketsFragment(),
            navOptions {
                popUpTo = R.id.nav_graph
                anim {
                    enter = if (animate) R.anim.launch_enter else 0
                    exit = if (animate) R.anim.launch_exit else 0
                }
            }
        )
    }
}