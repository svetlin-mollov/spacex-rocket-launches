package com.spacexrocketlaunches.module.welcome.ui

data class WelcomeState(
    val shouldShow: Boolean = false,
    val showOnNextStartChecked: Boolean = true,
)