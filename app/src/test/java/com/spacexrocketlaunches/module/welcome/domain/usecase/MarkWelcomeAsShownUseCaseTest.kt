package com.spacexrocketlaunches.module.welcome.domain.usecase

import com.google.common.truth.Truth.assertThat
import com.spacexrocketlaunches.module.welcome.domain.WelcomeRepository
import org.junit.Test

class MarkWelcomeAsShownUseCaseTest {

    @Test
    fun `GIVEN Welcome is not marked as shown WHEN MarkWelcomeAsShownUseCase is executed THEN the Welcome is marked as shown`() {

        /* Given */

        val repository = object : WelcomeRepository {
            override var isWelcomeShown: Boolean = false
        }

        val useCase = MarkWelcomeAsShownUseCase(repository)

        /* When */

        useCase.invoke(false)

        /* Then */

        assertThat(repository.isWelcomeShown).isTrue()
    }
}