package com.spacexrocketlaunches.module.welcome.domain.usecase

import com.google.common.truth.Truth.assertThat
import com.spacexrocketlaunches.module.welcome.domain.WelcomeRepository
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class ShouldShowWelcomeUseCaseTest {

    @Test
    fun `GIVEN welcome is not shown WHEN ShouldShowWelcomeUseCase is executed THEN true is returned`() {

        /* Given */

        val fakeRepository = mock<WelcomeRepository> {
            whenever(mock.isWelcomeShown).thenReturn(false)
        }

        val useCase = ShouldShowWelcomeUseCase(fakeRepository)

        /* When */

        val result = useCase.invoke()

        /* Then */

        assertThat(result).isTrue()
    }

    @Test
    fun `GIVEN welcome is shown WHEN ShouldShowWelcomeUseCase is executed THEN false is returned`() {

        /* Given */

        val fakeRepository = mock<WelcomeRepository> {
            whenever(mock.isWelcomeShown).thenReturn(true)
        }

        val useCase = ShouldShowWelcomeUseCase(fakeRepository)

        /* When */

        val result = useCase.invoke()

        /* Then */

        assertThat(result).isFalse()
    }
}