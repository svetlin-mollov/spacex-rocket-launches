package com.spacexrocketlaunches.module.launches.domain.usecase

import com.google.common.truth.Truth.assertThat
import com.mollov.util.Result
import com.spacexrocketlaunches.module.launches.domain.LaunchesRepository
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class GetRocketLaunchesUseCaseTest {

    @Test
    fun `GIVEN a repository with fixed list of launches WHEN GetRocketLaunchesUseCase is executed THEN the same list is returned`() {
        runBlockingTest {

            /* Given */

            val rocketId = "someId"

            val launches = listOf<Launch>(mock(), mock(), mock())

            val launchesRepository = mock<LaunchesRepository> {
                whenever(mock.getLaunches(rocketId)).thenReturn(Result.Success(launches))
            }

            val useCase = GetRocketLaunchesUseCase(launchesRepository)

            /* When */

            val result = useCase(rocketId)

            /* Then */

            val data = (result as Result.Success).data

            assertThat(data.size).isEqualTo(launches.size)
        }
    }
}