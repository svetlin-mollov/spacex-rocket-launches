package com.spacexrocketlaunches.module.launches.datasource.remote

import com.google.common.truth.Truth
import com.spacexrocketlaunches.module.launches.datasource.remote.dto.LaunchDto
import com.spacexrocketlaunches.module.launches.datasource.remote.dto.LinksDto
import com.spacexrocketlaunches.module.launches.datasource.remote.dto.PatchDto
import org.junit.Test

class LaunchMapperTest {

    @Test
    fun `GIVEN a fake LaunchDto WHEN it is mapped to Launch via LaunchMapper THEN the correct Launch object is returned`() {

        /* Given */

        val launchDto = LaunchDto(
            id = "1",
            name = "2",
            details = "3",
            launchDate = "4",
            success = false,
            rocketId = "5",
            links = LinksDto(PatchDto("6"))
        )

        val mapper = LaunchMapper()

        /* When */

        val result = mapper.mapFromLaunchDto(launchDto)

        /* Then */

        Truth.assertThat(result.id).isEqualTo(launchDto.id)
        Truth.assertThat(result.name).isEqualTo(launchDto.name)
        Truth.assertThat(result.details).isEqualTo(launchDto.details)
        Truth.assertThat(result.launchDate).isEqualTo(launchDto.launchDate)
        Truth.assertThat(result.success).isEqualTo(launchDto.success)
        Truth.assertThat(result.rocketId).isEqualTo(launchDto.rocketId)
        Truth.assertThat(result.missionPatchUrl).isEqualTo(launchDto.links?.patch?.small)
    }
}