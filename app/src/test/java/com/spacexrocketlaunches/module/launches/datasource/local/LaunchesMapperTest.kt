package com.spacexrocketlaunches.module.launches.datasource.local

import com.google.common.truth.Truth
import com.spacexrocketlaunches.module.launches.datasource.local.entity.LaunchEntity
import com.spacexrocketlaunches.module.launches.domain.model.Launch
import org.junit.Test

class LaunchMapperTest {

    @Test
    fun `GIVEN a fake LaunchEntity WHEN it is mapped to Launch via LaunchMapper THEN the correct Launch object is returned`() {

        /* Given */

        val launchEntity = LaunchEntity(
            id = "1",
            name = "2",
            details = "3",
            launchDate = "4",
            success = false,
            rocketId = "5",
            missionPatchUrl = "6"
        )

        val mapper = LaunchMapper()

        /* When */

        val result = mapper.mapFromLaunchEntity(launchEntity)

        /* Then */

        Truth.assertThat(result.id).isEqualTo(launchEntity.id)
        Truth.assertThat(result.name).isEqualTo(launchEntity.name)
        Truth.assertThat(result.details).isEqualTo(launchEntity.details)
        Truth.assertThat(result.launchDate).isEqualTo(launchEntity.launchDate)
        Truth.assertThat(result.success).isEqualTo(launchEntity.success)
        Truth.assertThat(result.rocketId).isEqualTo(launchEntity.rocketId)
        Truth.assertThat(result.missionPatchUrl).isEqualTo(launchEntity.missionPatchUrl)
    }

    @Test
    fun `GIVEN a fake Launch WHEN it is mapped to LaunchEntity via LaunchMapper THEN the correct LaunchEntity object is returned`() {

        /* Given */

        val rocket = Launch(
            id = "1",
            name = "2",
            details = "3",
            launchDate = "4",
            success = false,
            rocketId = "5",
            missionPatchUrl = "6"
        )

        val mapper = LaunchMapper()

        /* When */

        val result = mapper.mapToLaunchEntity(rocket)

        /* Then */

        Truth.assertThat(result.id).isEqualTo(rocket.id)
        Truth.assertThat(result.name).isEqualTo(rocket.name)
        Truth.assertThat(result.details).isEqualTo(rocket.details)
        Truth.assertThat(result.launchDate).isEqualTo(rocket.launchDate)
        Truth.assertThat(result.success).isEqualTo(rocket.success)
        Truth.assertThat(result.rocketId).isEqualTo(rocket.rocketId)
        Truth.assertThat(result.missionPatchUrl).isEqualTo(rocket.missionPatchUrl)
    }
}