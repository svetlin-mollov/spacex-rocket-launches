package com.spacexrocketlaunches.module.rockets.domain.usecase

import com.google.common.truth.Truth.assertThat
import com.mollov.util.Result
import com.spacexrocketlaunches.module.rockets.domain.RocketsRepository
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class GetRocketsUseCaseTest {

    @Test
    fun `GIVEN a repository with fixed list of rockets WHEN GetRocketsUseCase is executed THEN the same list is returned`() {
        runBlockingTest {

            /* Given */

            val rockets = listOf<Rocket>(mock(), mock())

            val repository = mock<RocketsRepository> {
                whenever(mock.getRockets()).thenReturn(Result.Success(rockets))
            }

            val useCase = GetRocketsUseCase(repository)

            /* When */

            val result = useCase.invoke()

            /* Then */

            assertThat(result).isInstanceOf(Result.Success::class.java)

            result as Result.Success

            assertThat(result.data).hasSize(rockets.size)
            assertThat(result.data.first()).isEqualTo(rockets.first())
        }
    }
}