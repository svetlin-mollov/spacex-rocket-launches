package com.spacexrocketlaunches.module.rockets.datasource.remote

import com.google.common.truth.Truth.assertThat
import com.spacexrocketlaunches.module.rockets.datasource.remote.dto.EnginesDto
import com.spacexrocketlaunches.module.rockets.datasource.remote.dto.LengthDto
import com.spacexrocketlaunches.module.rockets.datasource.remote.dto.RocketDto
import com.spacexrocketlaunches.module.rockets.datasource.remote.dto.WeightDto
import org.junit.Test

class RocketsMapperTest {

    @Test
    fun `GIVEN a fake RocketDto WHEN it is mapped to Rocket via RocketsMapper THEN the correct Rocket object is returned`() {

        /* Given */

        val rocketDto = RocketDto(
            id = "1",
            name = "2",
            country = "3",
            description = "44",
            wikipedia = "55",
            height = LengthDto(1.0, 2.0),
            diameter = LengthDto(3.0, 4.0),
            mass = WeightDto(5.0, 6.0),
            engines = EnginesDto(7),
            firstFlight = "4",
            isActive = true,
            flickrImages = listOf("5"),
        )

        val mapper = RocketsMapper()

        /* When */

        val result = mapper.mapFromRocketDto(rocketDto)

        /* Then */

        assertThat(result.id).isEqualTo(rocketDto.id)
        assertThat(result.name).isEqualTo(rocketDto.name)
        assertThat(result.country).isEqualTo(rocketDto.country)
        assertThat(result.description).isEqualTo(rocketDto.description)
        assertThat(result.wikipedia).isEqualTo(rocketDto.wikipedia)
        assertThat(result.height.meters).isEqualTo(rocketDto.height?.meters)
        assertThat(result.height.feet).isEqualTo(rocketDto.height?.feet)
        assertThat(result.diameter.meters).isEqualTo(rocketDto.diameter?.meters)
        assertThat(result.diameter.feet).isEqualTo(rocketDto.diameter?.feet)
        assertThat(result.mass.kg).isEqualTo(rocketDto.mass?.kg)
        assertThat(result.mass.lb).isEqualTo(rocketDto.mass?.lb)
        assertThat(result.enginesCount).isEqualTo(rocketDto.engines?.number)
        assertThat(result.firstFlight).isEqualTo(rocketDto.firstFlight)
        assertThat(result.isActive).isEqualTo(rocketDto.isActive)
    }
}