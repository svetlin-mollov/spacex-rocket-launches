package com.spacexrocketlaunches.module.rockets.domain.usecase

import com.google.common.truth.Truth.assertThat
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class FilterActiveRocketsUseCaseTest {

    @Test
    fun `GIVEN a list of active and inactive rockets WHEN FilterActiveRocketsUseCase is executed with true THEN only active rockets are returned`() {

        /* Given */

        val rockets = listOf<Rocket>(
            mock { whenever(mock.isActive).thenReturn(true) },
            mock { whenever(mock.isActive).thenReturn(false) },
        )

        val useCase = FilterActiveRocketsUseCase()

        /* When */

        val result = useCase.invoke(rockets, true)

        /* Then */

        assertThat(result.filter { it.isActive }).isNotEmpty()
        assertThat(result.filter { !it.isActive }).isEmpty()
    }

    @Test
    fun `GIVEN a list of active and inactive rockets WHEN FilterActiveRocketsUseCase is executed with false THEN all rockets are returned`() {

        /* Given */

        val rockets = listOf<Rocket>(mock(), mock())

        val useCase = FilterActiveRocketsUseCase()

        /* When */

        val result = useCase.invoke(rockets, false)

        /* Then */

        assertThat(result.size).isEqualTo(rockets.size)
    }

    @Test
    fun `GIVEN a list of active and inactive rockets WHEN FilterActiveRocketsUseCase is executed with null THEN all rockets are returned`() {

        /* Given */

        val rockets = listOf<Rocket>(mock(), mock())

        val useCase = FilterActiveRocketsUseCase()

        /* When */

        val result = useCase.invoke(rockets, null)

        /* Then */

        assertThat(result.size).isEqualTo(rockets.size)
    }
}