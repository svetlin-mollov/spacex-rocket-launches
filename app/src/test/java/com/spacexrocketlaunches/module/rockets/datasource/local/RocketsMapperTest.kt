package com.spacexrocketlaunches.module.rockets.datasource.local

import com.google.common.truth.Truth
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.Diameter
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.Height
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.Mass
import com.spacexrocketlaunches.module.rockets.datasource.local.entity.RocketEntity
import com.spacexrocketlaunches.module.rockets.domain.model.Length
import com.spacexrocketlaunches.module.rockets.domain.model.Rocket
import com.spacexrocketlaunches.module.rockets.domain.model.Weight
import org.junit.Test

class RocketsMapperTest {

    @Test
    fun `GIVEN a fake RocketEntity WHEN it is mapped to Rocket via RocketsMapper THEN the correct Rocket object is returned`() {

        /* Given */

        val rocketEntity = RocketEntity(
            id = "1",
            name = "2",
            country = "3",
            description = "44",
            wikipedia = "55",
            height = Height(1.0, 2.0),
            diameter = Diameter(3.0, 4.0),
            mass = Mass(5.0, 6.0),
            enginesCount = 1,
            firstFlight = "4",
            isActive = true,
            image = "5",
        )

        val mapper = RocketsMapper()

        /* When */

        val result = mapper.mapFromRocketEntity(rocketEntity)

        /* Then */

        Truth.assertThat(result.id).isEqualTo(rocketEntity.id)
        Truth.assertThat(result.name).isEqualTo(rocketEntity.name)
        Truth.assertThat(result.country).isEqualTo(rocketEntity.country)
        Truth.assertThat(result.description).isEqualTo(rocketEntity.description)
        Truth.assertThat(result.wikipedia).isEqualTo(rocketEntity.wikipedia)
        Truth.assertThat(result.height.meters).isEqualTo(rocketEntity.height.heightMeters)
        Truth.assertThat(result.height.feet).isEqualTo(rocketEntity.height.heightFeet)
        Truth.assertThat(result.diameter.meters).isEqualTo(rocketEntity.diameter.diameterMeters)
        Truth.assertThat(result.diameter.feet).isEqualTo(rocketEntity.diameter.diameterFeet)
        Truth.assertThat(result.mass.kg).isEqualTo(rocketEntity.mass.massKg)
        Truth.assertThat(result.mass.lb).isEqualTo(rocketEntity.mass.massLb)
        Truth.assertThat(result.enginesCount).isEqualTo(rocketEntity.enginesCount)
        Truth.assertThat(result.firstFlight).isEqualTo(rocketEntity.firstFlight)
        Truth.assertThat(result.isActive).isEqualTo(rocketEntity.isActive)
    }

    @Test
    fun `GIVEN a fake Rocket WHEN it is mapped to RocketEntity via RocketsMapper THEN the correct RocketEntity object is returned`() {

        /* Given */

        val rocket = Rocket(
            id = "1",
            name = "2",
            country = "3",
            description = "44",
            wikipedia = "55",
            height = Length(1.0, 2.0),
            diameter = Length(3.0, 4.0),
            mass = Weight(5.0, 6.0),
            enginesCount = 1,
            firstFlight = "4",
            isActive = true,
            image = "5",
        )

        val mapper = RocketsMapper()

        /* When */

        val result = mapper.mapToRocketEntity(rocket)

        /* Then */

        Truth.assertThat(result.id).isEqualTo(rocket.id)
        Truth.assertThat(result.name).isEqualTo(rocket.name)
        Truth.assertThat(result.country).isEqualTo(rocket.country)
        Truth.assertThat(result.description).isEqualTo(rocket.description)
        Truth.assertThat(result.wikipedia).isEqualTo(rocket.wikipedia)
        Truth.assertThat(result.height.heightMeters).isEqualTo(rocket.height.meters)
        Truth.assertThat(result.height.heightFeet).isEqualTo(rocket.height.feet)
        Truth.assertThat(result.diameter.diameterMeters).isEqualTo(rocket.diameter.meters)
        Truth.assertThat(result.diameter.diameterFeet).isEqualTo(rocket.diameter.feet)
        Truth.assertThat(result.mass.massKg).isEqualTo(rocket.mass.kg)
        Truth.assertThat(result.mass.massLb).isEqualTo(rocket.mass.lb)
        Truth.assertThat(result.enginesCount).isEqualTo(rocket.enginesCount)
        Truth.assertThat(result.firstFlight).isEqualTo(rocket.firstFlight)
        Truth.assertThat(result.isActive).isEqualTo(rocket.isActive)
    }
}