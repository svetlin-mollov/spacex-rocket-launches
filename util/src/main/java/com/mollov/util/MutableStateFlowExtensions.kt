package com.mollov.util

import kotlinx.coroutines.flow.MutableStateFlow

suspend inline fun <T> MutableStateFlow<T>.change(crossinline modifier: (T) -> T) {
    emit(modifier(value))
}