package com.mollov.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

inline fun LifecycleOwner.whileIn(
    state: Lifecycle.State,
    crossinline block: suspend CoroutineScope.() -> Unit,
) {
    lifecycle.coroutineScope.launch {
        lifecycle.repeatOnLifecycle(state) {
            block()
        }
    }
}