package com.mollov.util

import android.content.res.Resources

val Int.dp: Int
    get() = dpF.toInt()

val Int.dpF: Float
    get() = this * Resources.getSystem().displayMetrics.density
