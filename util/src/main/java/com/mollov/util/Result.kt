package com.mollov.util

sealed class Result<out R> {

    class Success<out T>(val data: T) : Result<T>()

    class Error(val error: Throwable) : Result<Nothing>()
}