package com.mollov.util

inline fun <T, R> List<T>.contentEqualsBy(
    other: List<T>,
    crossinline selector: (T) -> R?
): Boolean {

    if (this.size != other.size) return false

    for (i in 0 until size) {
        if (selector(this[i]) != selector(other[i])) return false
    }

    return true
}

fun <T> List<T>.contentEquals(other: List<T>): Boolean = contentEqualsBy(other) { it }

fun <T> listBuilder(initialSize: Int = 10, builder: ListBuilder<T>.() -> Unit): List<T> {
    val items = ArrayList<T>(initialSize)

    val listBuilder = ListBuilder(items)

    listBuilder.builder()

    return listBuilder.list
}

class ListBuilder<T>(val list: MutableList<T>) {

    operator fun T.unaryPlus() {
        list.add(this)
    }
}