package com.mollov.util

/**
 * Utilises the fine-grained diff check between two objects of the same type
 */
class Diff<T>(private val new: T, private val old: T?) {

    fun <F> by(selector: (T) -> F?): Boolean =
        if (old == null) true else selector(new) != selector(old)

    fun <L : List<*>> byList(selector: (T) -> L?): Boolean {

        if (old == null) return true

        val oldList = selector(old)
        val newList = selector(new)

        if (oldList === newList) return false

        if (oldList == null) return newList != null

        if (newList == null) return true

        if (newList.size != oldList.size) return true

        val size = newList.size
        for (i in 0 until size) {
            if (newList[i] != oldList[i]) return true
        }

        return false
    }
}