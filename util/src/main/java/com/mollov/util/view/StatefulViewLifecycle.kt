package com.mollov.util.view

import com.mollov.util.Diff

interface StatefulViewLifecycle<State> : ViewLifecycle {

    fun render(state: State, diff: Diff<State>)
}