package com.mollov.util.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.FrameLayout

abstract class StatelessView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), ViewLifecycle {

    final override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        ensureView()

        onVisible()
    }

    final override fun onDetachedFromWindow() {

        onInvisible()

        super.onDetachedFromWindow()
    }

    final override fun onViewDestroyed(view: View) {
        throw UnsupportedOperationException("View does not have onDestroy() callback!")
    }

    final override fun onInitializeAccessibilityNodeInfo(info: AccessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(info)
        info.className = javaClass.simpleName
    }

    /**
     * Forces the view's creation. Otherwise, the view will be created on the first [onAttachedToWindow] call.
     *
     * If the view is already created, the method will return, so it is safe to call it multiple times, if you want to
     * ensure the view's existence.
     *
     * *Note:* Be sure to NOT call this method inside non-final methods, otherwise the view's creation may be performed
     * before the child class constructor and initialization blocks which may lead to unexpected behavior and
     * [NullPointerException]s
     *
     * @return `true` if the view was already created, `false` if the view was created by this call
     */
    protected fun ensureView(): Boolean {

        // Do nothing if the view is already created
        if (childCount > 0) return true

        addView(createView(LayoutInflater.from(context), this))
        onViewCreated(this)

        return false
    }
}