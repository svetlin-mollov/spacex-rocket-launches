package com.mollov.util.view

import android.content.Context
import android.util.AttributeSet
import com.mollov.util.Diff

abstract class StatefulView<State> @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : StatelessView(context, attrs, defStyleAttr), StatefulViewLifecycle<State> {

    /**
     * Changes the view's state. Be sure to call this on the Main thread
     *
     * @see postState
     */
    var state: State? = null
        set(value) {

            if (value == null || field === value) return

            val diff = Diff(value, field)

            field = value

            ensureView()

            render(value, diff)
        }

    /**
     * Posts a state change on the Main thread
     */
    @Suppress("MemberVisibilityCanBePrivate")
    fun postState(state: State) {
        post { this.state = state }
    }
}