package com.mollov.util.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

interface ViewLifecycle {

    fun createView(inflater: LayoutInflater, parent: ViewGroup): View

    fun onViewCreated(view: View) {}

    fun onVisible() {}

    fun onInvisible() {}

    fun onViewDestroyed(view: View) {}
}