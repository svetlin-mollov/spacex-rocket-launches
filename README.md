# SpaceX Rocket Launches

## About the app

Explore the SpaceX rockets, find their passed missions and know when the next launches are planned to happen

### The idea

![Design](images/Design.png)

### The result

Supports Light and Dark mode, and... Offline mode

![SsWelcome1](images/SsWelcome1.png) ![SsWelcome2](images/SsWelcome2.png)

![SsMain](images/SsMain.png)

![SsDetails](images/SsDetails.png)

## The dev process

I believe in the user-centric development process. The users should be able to experience new app features as soon as they are ready and the developers should monitor how their code is being used and verify that the direction they are headed is correct and if not, make adjustments.

SpaceX Rocket Launches development process is structured around these beliefs. The project is designed with CI/CD from the beginning. This enables frequent app releases with confidence in the app's quality since all major functionalities are covered in Unit and UI tests, and nothing gets publishes until the tests pass.

Each new feature or improvement is developed following these steps:

1. Create a new branch from `master`. Usually, there is an issue tracking system that generates a unique number for each issue. The new branch is prefixed with that number which creates a relation to the issue containing the business value and the functional requirements of the task
2. Write the source code to satisfy the functional requirements. In this step, the developer communicates with the other stakeholders intensively to build a high-quality feature
3. Write the tests that verify the correctness of the code written in point 2. This step can sometimes precede point 2 in the so-called TDD
4. Create a pull request to the `master` branch. In this step, another member of the team reviews the code written by you. It also triggers an automatic pipeline that verifies that the app builds and the tests pass. The pull request can not be merged until the pipeline succeeds

## The technology

MVVM, based on Koltin Coroutines Flows

![Architecture](images/Architecture.png)

## How to run the app?

The simplest way to run the app is to open the root directory in Android Studio and press the green "play" button from the toolbar.

## Things to improve

- Polish the UX - Do the Shared element transition, format the dates!!!, show all rocket images
- Localisation
- Extend Unit tests - test the data sources and the ViewModels
- Add UI tests

## Resources

- <div>Icons made by <a href="https://www.flaticon.com/authors/icongeek26" title="Icongeek26">Icongeek26</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
- <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
- <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

## Licence

Copyright 2021 Svetlin Mollov

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

